#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include <pcl_ros/transforms.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <visualization_msgs/Marker.h>
#include <pcl/filters/passthrough.h>


#include <sstream>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_values(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr tf_values(new pcl::PointCloud<pcl::PointXYZ>);
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);

ros::Publisher tf_pub;
tf::TransformListener *tf_listener; 
tf::StampedTransform transform;



void grid_occupancy_calculate(pcl::PointCloud<pcl::PointXYZ>::Ptr translated_values)
{
     pcl::PassThrough<pcl::PointXYZ> pass;
    int counter[12][8] = {0};
    int x,y;

    pass.setInputCloud(translated_values);

    pass.setFilterFieldName("x");
    pass.setFilterLimits(-6.0, 6.0);
    pass.filter(*filtered_values);
    pass.setInputCloud(filtered_values);

    pass.setFilterFieldName("y");
    pass.setFilterLimits(-4.0, 4.0);
    pass.filter(*filtered_values);
    pass.setInputCloud(filtered_values);

    pass.setFilterFieldName("z");
    pass.setFilterLimits(-10, 0.3);
    pass.filter(*filtered_values);

}

void callback(const sensor_msgs::PointCloud2::ConstPtr& msg) 
{

    pcl::fromROSMsg (*msg, *cloud);
    PointCloud pcl_out;
    ROS_INFO ("Cloud: width = %d, height = %d\n", cloud->width, cloud->height);
    BOOST_FOREACH (const pcl::PointXYZ& pt, cloud->points);
    }



int main(int argc, char** argv)
{
    int counter [12] [8] = {0};
    ros::init(argc, argv, "sub_pcl");
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2>("/points_raw", 1, callback);
    tf_pub = nh.advertise<PointCloud> ("tf_points2", 1);

    tf_listener    = new tf::TransformListener();
     ros::Publisher marker_pub = nh.advertise<visualization_msgs::Marker>("/rviz", 1);

  while (ros::ok())
  {
    // Publish the marker
    tf_listener->waitForTransform("/base_link", "velodyne", ros::Time(0), ros::Duration(3.0));
    int marker_id = 0;
    
    pcl_ros::transformPointCloud(*cloud, *tf_values, transform);
    
    grid_occupancy_calculate(tf_values);
    
    for (int i = 0; i< filtered_values->points.size(); i++) {
        int x,y;
        x = floor(filtered_values ->points[i].x + 6);
        y = floor(filtered_values ->points[i].y + 4);
        counter[x][y]++;
    }

    for (int i = 0; i < 12; ++i)
    {
        for(int j = 0; j < 8; ++j)
        {
            visualization_msgs::Marker marker;
            // Set the frame ID and timestamp.  See the TF tutorials for information on these.
            marker.header.frame_id = "/base_link";
            marker.header.stamp = ros::Time::now();

            // Set the namespace and id for this marker.  This serves to create a unique ID
            // Any marker sent with the same namespace and id will overwrite the old one
            marker.ns = "basic_shapes";

            // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
            marker.action = visualization_msgs::Marker::ADD;
            marker.type= visualization_msgs::Marker::CUBE;

            // Set the color -- be sure to set alpha to something non-zero!
            marker.lifetime = ros::Duration();

            // Set the scale of the marker -- 1x1x1 here means 1m on a side
            marker.scale.x = 0.5;
            marker.scale.y = 0.5;
            marker.scale.z = 0.5;


            marker.pose.orientation.x = 0.0;
            marker.pose.orientation.y = 0.0;
            marker.pose.orientation.z = 0.0;
            marker.pose.orientation.w = 1.0;
           
            marker.id = marker_id % 96;
            marker_id ++;
            marker.pose.position.x = i - 6;
            marker.pose.position.y = j - 4;
            marker.color.r = 0.0f;
            marker.color.g = 1.0f;
            marker.color.b = 0.0f;
            marker.color.a = counter[i][j]/300;

            marker_pub.publish(marker);

            counter[i][j] = 0;
        }
        

    }


    ros::spinOnce();
  }    
    return 0;

}

